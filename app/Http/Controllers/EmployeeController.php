<?php

namespace App\Http\Controllers;

use App\Services\EmployeeService;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;

use Exception;

class EmployeeController extends Controller
{
    private $service;

    public function __construct(EmployeeService $employeeService)
    {
        $this->service = $employeeService;
    }

    public function get(): JsonResponse
    {
        return $this->service->findByUser($this->userId());
    }

    public function show(int $id): JsonResponse
    {
        return $this->service->find($id, $this->userId());
    }

    public function destroy(int $id): JsonResponse
    {
        return $this->service->destroy($id, $this->userId());
    }

    public function import(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'csv' => 'required|file|mimes:csv,txt',
        ]);

        if($validator->fails()){
            return response()->json($validator->messages(), 422);
        }
        
        return $this->service->import($request->file('csv'), $this->user());
    }
}
