<?php

namespace App\Imports;

use App\Repositories\Contracts\EmployeeRepositoryInterface;

use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Collection;

use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Support\Facades\Mail;
use App\Mail\EmailImportError;

class EmployeesImport implements ToCollection, WithBatchInserts, WithValidation, WithChunkReading, WithEvents, WithHeadingRow, ShouldQueue
{
    use Importable;

    protected $user;
    protected $employee;

    public function __construct(object $user, EmployeeRepositoryInterface $employeeRepositoryInterface) {
        $this->user     = $user;
        $this->employee = $employeeRepositoryInterface;
    }

    public function collection(Collection $rows)
    {   
        foreach ($rows as $row) 
        {
            $employee = $this->employee->findByName($row['name']);

            if(!$employee){
                $this->employee->create([
                    'user_id'    => $this->user->id,
                    'name'       => $row['name'],
                    'email'      => $row['email'],
                    'document'   => $row['document'],
                    'city'       => $row['city'],
                    'state'      => $row['state'],
                    'start_date' => $row['start_date']
                ]);
            }else{
                $this->employee->update($employee->id,[
                    'user_id'    => $this->user->id,
                    'name'       => $row['name'],
                    'email'      => $row['email'],
                    'document'   => $row['document'],
                    'city'       => $row['city'],
                    'state'      => $row['state'],
                    'start_date' => $row['start_date']
                ]);
            }

        }
    }

    public function rules(): array
    {
        return [
            'name'        => 'required|string|max:255',
            'email'       => 'required|string|max:255',
            'document'    => 'required|max:14',
            'city'        => 'required|string|max:255',
            'state'       => 'required|string|max:2',
            'start_date'  => 'required|date_format:Y-m-d'
        ];
    }

    public function headingRow(): int
    {
        return 1;
    }

    public function chunkSize(): int
    {
        return 200;
    }

    public function batchSize(): int
    {
        return 200;
    }

    public function registerEvents(): array
    {
        return [
            ImportFailed::class => function(ImportFailed $event) {
                Mail::to($this->user->email)->queue(new EmailImportError());
            },
        ];
    }

}
