<?php

namespace App\Repositories;

use App\Repositories\Contracts\EmployeeRepositoryInterface;

use App\Models\Employee;

class EmployeeRepository implements EmployeeRepositoryInterface
{
	private $model;

	public function __construct(Employee $model)
	{
		$this->model = $model;
	}

	public function findByUser(int $userId): ?object
	{
		return $this->model->where('user_id', $userId)->get();
	}    

	public function find(int $id, int $userId): ?object
	{
		return $this->model->where([
			'id'      => $id,
			'user_id' => $userId
		])->first();
	}

	public function destroy(int $id, int $userId): bool
	{
		return $this->model->where([
			'id'      => $id,
			'user_id' => $userId
		])->delete();
	}

	public function findByName(string $name): ?object
	{
		return $this->model->where('name', $name)->first();
	}

	public function update(int $id, array $attributes): bool
	{
		return $this->model->where('id', $id)->update($attributes);
	}

	public function create(array $employee): Employee
	{
		return $this->model->create($employee);
	}
}