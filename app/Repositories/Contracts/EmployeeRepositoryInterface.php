<?php

namespace App\Repositories\Contracts;

interface EmployeeRepositoryInterface 
{
    public function findByUser(int $userId);
    public function find(int $id, int $userId);
    public function destroy(int $id, int $userId);
    public function findByName(string $name);
    public function update(int $id, array $attributes);
    public function create(array $employee);
}