<?php

namespace App\Services;

use App\Repositories\Contracts\EmployeeRepositoryInterface;

use Illuminate\Http\JsonResponse;

use App\Imports\EmployeesImport;

use App\Jobs\NotifyUserOfCompletedImportJob;

use Exception;

class EmployeeService
{
    private $employee;

    public function __construct(EmployeeRepositoryInterface $employeeRepositoryInterface)
    {
        $this->employee = $employeeRepositoryInterface;
    }

    public function findByUser(int $userId): JsonResponse
    {
        try {
            $employees = $this->employee->findByUser($userId);
            if(count($employees) == 0){
                return response()->json([
                    'message' => 'no employees were found.'
                ], 404);
            }
        } catch (Exception $e) {
            return response()->json([
                'message' => 'internal exception, try again later.'
            ], 500);
        }

        return response()->json([
            'message' => 'employees found successfully.',
            'data'    => $employees
        ], 200);
    }

    public function find(int $id, int $userId): JsonResponse
    {
        try {
            $employee = $this->employee->find($id, $userId);
            if(!$employee){
                return response()->json([
                    'message' => 'employee not found.'
                ], 404);
            }
        } catch (Exception $e) {
            return response()->json([
                'message' => 'internal exception, try again later.'
            ], 500);
        }

        return response()->json([
            'message' => 'employee found successfully.',
            'data'    => $employee
        ], 200);
    }

    public function destroy(int $id, int $userId): JsonResponse
    {
        try {
            $employee = $this->employee->find($id, $userId);
            if(!$employee){
                return response()->json([
                    'message' => 'employee not found.'
                ], 404);
            }
            $this->employee->destroy($id, $userId);
        } catch (Exception $e) {
            return response()->json([
                'message' => 'internal exception, try again later.'
            ], 500);
        }

        return response()->json([
            'message' => 'employee successfully removed.'
        ], 200);
    }

    public function import(object $file, object $user): JsonResponse
    {
        try {
            (new EmployeesImport($user, $this->employee))->queue($file)->chain([
                new NotifyUserOfCompletedImportJob($user->email),
            ]); 
        } catch (Exception $e) {
            return response()->json([
                'message' => 'an unexpected error occurred in the import process.'
            ], 500);
        }

        return response()->json([
            'message' => 'the file will be processed in a moment.'
        ], 201);
    }
}
