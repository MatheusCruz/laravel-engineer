<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class EmployeeServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            'App\Repositories\Contracts\EmployeeRepositoryInterface',
            'App\Repositories\EmployeeRepository'
        );
    }
}