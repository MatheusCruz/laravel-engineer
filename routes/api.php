<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api', 'namespace' => 'App\Http\Controllers'], function () {
    
    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::prefix('employees')->group(function () {

        Route::get('/', [
            'as'   => 'employees.get',
            'uses' => 'EmployeeController@get'
        ]);

        Route::get('/{id}', [
            'as'   => 'employees.show',
            'uses' => 'EmployeeController@show'
        ]);

        Route::delete('/{id}', [
            'as'   => 'employees.destroy',
            'uses' => 'EmployeeController@destroy'
        ]);

        Route::post('/', [
            'as'   => 'employees.import',
            'uses' => 'EmployeeController@import'
        ]);

    });

    Route::get('/teste', function (Request $request) {
        return 'teste';
    })->name('teste');
  
});
